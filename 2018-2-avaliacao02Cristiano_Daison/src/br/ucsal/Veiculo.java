package br.ucsal;

public abstract class Veiculo implements Comparable<Veiculo>{
	

	private String placa;
	private String modelo;
	private Integer Ano_fabric;
	private Double valor;

	public Veiculo(String placa, String modelo, Integer Ano_fabric, Double valor) throws ValorNaoValidoException {
		this.Ano_fabric = Ano_fabric;
		this.modelo = modelo;
		this.placa = placa;
		if (valor <= 0) {

			throw new ValorNaoValidoException("valor invalido");
					
				} else {
					this.valor = valor;
				}
			}
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAno_fabric() {
		return Ano_fabric;
	}

	public void setAno_fabric(Integer ano_fabric) {
		Ano_fabric = ano_fabric;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws ValorNaoValidoException {
		if (valor <= 0) {

	throw new ValorNaoValidoException("valor invalido");
			
		} else {
			this.valor = valor;
		}
	}

	@Override
	public String toString() {
		return "Locadora [placa=" + placa + ", modelo=" + modelo + ", Ano_fabric=" + Ano_fabric + ", valor=" + valor
				+ "]";
	}

	public abstract int CalcularCustoLocacao();

}
