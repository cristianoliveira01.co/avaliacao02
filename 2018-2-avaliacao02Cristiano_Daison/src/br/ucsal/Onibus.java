package br.ucsal;

public class Onibus extends Veiculo {
	private static final int CONT = 10;
	private Integer passageiros;

	public Onibus(String placa, String modelo, Integer Ano_fabric, Double valor, Integer passageiros) throws ValorNaoValidoException {
		super(placa, modelo, Ano_fabric, valor);
		this.passageiros = passageiros;

	}

	public Integer getPassageiros() {
		return passageiros;
	}

	public void setPassageiros(Integer passageiros) {
		this.passageiros = passageiros;
	}

	@Override
	public String toString() {
		return "Onibus [passageiros=" + passageiros + "]" 
				 + super.toString();
	}

	@Override
	public int CalcularCustoLocacao() {
		return this.passageiros * CONT;
	}

	@Override
	public int compareTo(Veiculo v) {
		
		return this.getPlaca().compareTo(v.getPlaca());
	}

}
