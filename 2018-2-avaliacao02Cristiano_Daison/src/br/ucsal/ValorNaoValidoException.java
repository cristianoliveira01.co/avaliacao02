package br.ucsal;

public class ValorNaoValidoException extends Exception {

	public ValorNaoValidoException(String mensagem){ 
		super(mensagem); 
	}
}
