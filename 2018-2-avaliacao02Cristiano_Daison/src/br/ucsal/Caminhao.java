package br.ucsal;

public class Caminhao extends Veiculo {
	private static final int CONT = 8;
	private Integer eixo;
	private Integer carga;

	public Caminhao(String placa, String modelo, Integer Ano_fabric, Double valor, Integer eixo, Integer carga) throws ValorNaoValidoException {
		super(placa, modelo, Ano_fabric, valor);
		this.carga = carga;
		this.eixo = eixo;
	}

	public Integer getEixo() {
		return eixo;
	}

	public void setEixo(Integer eixo) {
		this.eixo = eixo;
	}

	public Integer getCarga() {
		return carga;
	}

	public void setCarga(Integer carga) {
		this.carga = carga;
	}

	@Override
	public String toString() {
		return "Caminhao [eixo=" + eixo + ", carga=" + carga + "]" 
				+ super.toString();
	}

	@Override
	public int CalcularCustoLocacao() {
		return this.carga * CONT;

	}

	@Override
public int compareTo(Veiculo v) {
		
		return this.getPlaca().compareTo(v.getPlaca());
	}
	}


